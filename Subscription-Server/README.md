## SubscriptionServer

I greet you in asynchronous world, the chosen one!

### Task description

The task for you is to implement an asynchronous(not synchronous, it's important!) `boost.asio` TCP server.
The server receives a request in the json, send response and send message with specified delay to endpoint.

Your server must support the following command line arguments:

`-h [ --help ] ` - show help

`-p [ --port ] arg` - configure listening port

`-l [ --log-output ] arg` - configure name of logfile(also may be stdout, stderr)

Default port is `2080`.
Default logfile name is `log.txt`.

For parsing command line arguments you should use `boost.program_options`.
You need log Server start, also your server should log information about received requests in followed format.
```
[minutes:seconds:milliseconds since start]Request recieved:
        JSON
[minutes:seconds:milliseconds since start] Response sent:
        JSON 
```
You can log whatever you want, but the above events must be logged. Also, be sure to follow the logging 
template. Log operation should be also async.

You must implement server in `Server` directory.
Also, you must implement tests in `Test` directory using `gtest` and/or `gmock` for unit testing.
Implementation of client is optional, but it helps you to test your solution, also for integration tests.

You need create shell/bash/zsh script for building server and running tests.

Your server have 5 lvls

### I lvl
Your server should support subscription request. Example of subscription request.

    {
       "type": "subscribe",
       "endpoint": {
            "port": 1234,
            "ip": "127.0.0.1"
       },
       "delay": 10
       "message": "Hello Async World!"
    }

For parsing JSON you should use `boost.json`

The delay is 10 milliseconds in example.

The response in success case must be:

    {
        "code": subscription id,
        "info": "Subscription successful"
    }

Subscription id is implementation defined positive std::int64_t. Every subscription id should be unique. 
In fail case:

    {
        "code": -1,
        "info": reason for failure
    }

If subscription request was successful server should send `Hello Async World!` every 10ms to 127.0.0.1:1234.
Server should always be possible to receive requests.

### II lvl

Your server should process requests for update. User should be able to update delay, message and both:

    {
        "type": "update",
        "id": 123 
        "delay": 20
        "message": "Hello Updated Async World!"
    }

In case of failure(for instance 123 is nonexistent id) return code should be -1, also you need write reason of 
failure in info field. In case of success return code should be equal to the actual delay value:

    {
        "code": 20
        "info": "123 subscription successfully updated"
    }

### III lvl

Your server should be able to process unsubscription requests:

    {
        "type": "unsubscribe",
        "id": subscription id
    }

In fail case return code should be -1, also you need write reason of fail in info field.
In success case return code should be old subscription id.

    {
        "code": subscription id
        "info": "Unsubscription successful"
    }

### IV lvl
Your server should process shutdown requests:

    {
        "type": "shutdown",
        "password": password
    }

if password correct server should send response:

    {
        "code": number of subcribtions,
        "info": "Shutdown resquest successful"
    }

And send `The server is shutting down, bye!` to all subscribers. Otherwise, send response:

    {
        "code": -1,
        "info": "Invalid password"
    }

Add command line argument: 

`-s [ --password ] arg` - configure password.

It isn't cybersecure, but for educational goals it's fine.

### V lvl (optional)

Your server should support multithreading.
Add `-j [ --thread ] arg` argument, which mean number of threads. Possible value should be 1, 2, 4, 6 or 8.

## Important notes!
- Delay should be in range 5ms...10000ms.
- Maximum message length must be 1024 bytes.
- Log operation should be also async(non-blocking).
- You can extend the error system. All codes less than 0 by the client will be perceived as an error. 

## Useful links
- [Talking Async Ep1: Why C++20 is the Awesomest Language for Network Programming](https://www.youtube.com/watch?v=icgnqFM-aY4)

Following text helps you to implement solution.

## Maintainer's guide

### Testing
- [ ] Install packages required for build solution.
- [ ] Run `build.sh`. Observe no errors.
- [ ] Run Server with password, after that run prepared test with the same password. All tests must pass. // TODO

### General Requirements
Solution should use:
- [ ] compile option `-std=c++20` or `-std=c++17`
- [ ] compile options `-Wall -Wextra`
- [ ] CMake - for building
- [ ] .gitignore isn't empty
- [ ] .clang-format isn't empty
- [ ] .clang-tidy isn't empty
- [ ] Code follow style defined in .clang-format 
- [ ] There must be no error produced by clang-tidy
- [ ] boost.asio
- [ ] Async asio functions(async_*)
- [ ] boost.program_options - For parsing command line arguments
- [ ] boost.json - for parsing json 
- [ ] Solution have unit tests
- [ ] gtest

### Common Requirements
- [ ] No raw pointers used and no potential memory leaks exist
- [ ] Potential errors are wrapped in try-catch
- [ ] Header guards are present

### Optional Requirements
- [ ] compile option `Wpedantic`
- [ ] Modern CMake features(target_*)
- [ ] coroutines (co_spawn, co_await, co_return, awaitable<T>)
- [ ] Solution have integration tests
- [ ] gmock
- [ ] Solution supports multithreading(cli argument -j supported)

### The Main Requirement
- [ ] You enjoy this solution